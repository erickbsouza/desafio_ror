Rails.application.routes.draw do

  resources :projects
  resources :cashiers
  resources :events

  get 'site/home', to: "site#home", as: "home"
  get 'site/dados', to: "site#dados", as: "dados"
  get 'inicial/index', to: "inicial#index", as: "index"


  devise_for :admins
  
  devise_for :users, controllers: { sessions: 'users/sessions', registrations: 'users/registrations' }

  #devise_scope :user do get "/some/route" => "some_devise_controller"


  authenticated :user do
    root 'site#home', as: :authenticated_root
  end

  authenticated :admin do
    root 'site#secret', as: :authenticated_root2
    #post '/delete_user/:id', to
  end

  root 'inicial#index'
  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html
end
