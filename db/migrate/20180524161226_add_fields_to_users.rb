class AddFieldsToUsers < ActiveRecord::Migration[5.1]
  def change
  	add_column :users, :name, :string
    add_column :users, :foto, :string
    add_column :users, :gender, :string
    add_column :users, :nascimento, :date
    add_column :users, :cargo, :string
  end
end
