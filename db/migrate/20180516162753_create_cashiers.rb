class CreateCashiers < ActiveRecord::Migration[5.1]
  def change
    create_table :cashiers do |t|
      t.string :name
      t.string :category
      t.text :description
      t.date :day
      t.decimal :value

      t.timestamps
    end
  end
end
