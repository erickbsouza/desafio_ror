# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rails db:seed command (or created alongside the database with db:setup).
#
# Examples:
#
#   movies = Movie.create([{ name: 'Star Wars' }, { name: 'Lord of the Rings' }])
#   Character.create(name: 'Luke', movie: movies.first)


num = 0

while num<10
	x = User.new
	x.email = "user#{num}@a.com"
	x.password = "123123123"
	x.name = "user #{num}"
	x.save
	num+=1
end

puts "fim da seed"