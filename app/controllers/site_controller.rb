class SiteController < ApplicationController
  layout 'admin_lte_2'

  def home
  	@projects = Project.order(start: :desc)
  	@events = Event.order(day: :desc)
  end

  def dados

  end

  def secret
  	@users = User.all
  end
  
end
