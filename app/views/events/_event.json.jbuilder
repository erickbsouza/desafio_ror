json.extract! event, :id, :name, :description, :day, :category, :created_at, :updated_at
json.url event_url(event, format: :json)
