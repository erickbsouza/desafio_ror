json.extract! cashier, :id, :name, :category, :description, :day, :value, :created_at, :updated_at
json.url cashier_url(cashier, format: :json)
